EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:TechnoBoard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title "TechnoBoard Model 1"
Date "Mon 23 mar 2015"
Rev "A"
Comp "Copyright 2015, Jean-Sébastien Castonguay"
Comment1 "Licensed under the Creative Commons Attribution-ShareAlike (BY-SA) license "
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 700  700  1550 1100
U 5358078D
F0 "MCU, Power supply and comm" 50
F1 "MCU.sch" 50
$EndSheet
$Sheet
S 700  2000 1600 1150
U 53581084
F0 "PMOD, LED and Button" 50
F1 "pmod.sch" 50
$EndSheet
Text Notes 7000 6450 0    118  ~ 0
For futher details, go to www.technoboard.ca
$EndSCHEMATC
