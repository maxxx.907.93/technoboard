technoboard readme by Jean-Sebastien Castonguay is licensed under a Creative
Commons Attribution-ShareAlike 4.0 International License.

TECHNOBOARD DOCUMENTATION
=========================

See full documentation at www.technoboard.ca.


RELEASE NOTE
============

2015-08-24 - Bugs fixed
	technoboard HW model 1     - RevA
	tblib                      - v1.0.0
	LabVIEW tblib              - v1.0.0
	DAQ&C LabVIEW application  - v1.0.0
	Embedded DAQ&C application - v1.0.0
	
	Moved the release folders to follow the web site.
	Fixed the GPIO LAT and PORT error when writting GPIO.
   	
2015-05-11 - Initial release
	technoboard HW model 1     - RevA
	tblib                      - v1.0.0
	LabVIEW tblib              - v1.0.0
	DAQ&C LabVIEW application  - v1.0.0
	Embedded DAQ&C application - v1.0.0

	Some issues are still opened but it is time now to release the package
	and see how it is going.
	
	Issue #19: Generate cross-platform installer
	Issue #17: Add echo to DAQ&C embedded application
	Issue #16: Error heandling in labview seams not always working when
	           receiving no message or an error message
	Issue #14: Allows opening more than 2 analog in.
	Issue #11: Reevaluate LF/CR from/to the technoboard
	Issue #10: Add "backspace" function to console module
	Issue #7:  Change bootloader VID/PID
	Issue #5:  PWM duty cycle must be more accurate than %
	Issue #2:  USB stack client must support more than 2 X 255 bytes
	
