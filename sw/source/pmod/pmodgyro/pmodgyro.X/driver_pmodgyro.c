/* The MIT License (MIT)

Copyright (c) 2015 Jean-Sebastien Castonguay

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

#include "driver_pmodgyro.h"
#include "technoboard.h"


/* Register adresses */
#define WHO_AM_I        0x0F
#define CTRL_REG1       0x20
#define CTRL_REG2       0x21
#define CTRL_REG3       0x22
#define CTRL_REG4       0x23
#define CTRL_REG5       0x24
#define REFERENCE       0x25
#define OUT_TEMP        0x26
#define STATUS_REG      0x27
#define OUT_X_L         0x28
#define OUT_X_H         0x29
#define OUT_Y_L         0x2A
#define OUT_Y_H         0x2B
#define OUT_Z_L         0x2C
#define OUT_Z_H         0x2D
#define FIFO_CTRL_REG   0x2E
#define FIFO_SRC_REG    0x2F
#define INT1_CFG        0x30
#define INT1_SRC        0x31
#define INT1_TSH_XH     0x32
#define INT1_TSH_XL     0x33
#define INT1_TSH_YH     0x34
#define INT1_TSH_YL     0x35
#define INT1_TSH_ZH     0x36
#define INT1_TSH_ZL     0x37
#define INT1_DURATION   0x38

/* CTRL_REG1 */
#define REG1_DR1        0x80
#define REG1_DR0        0x40
#define REG1_BW1        0x20
#define REG1_BW0        0x10
#define REG1_PD         0x08
#define REG1_ZEN        0x04
#define REG1_YEN        0x02
#define REG1_XEN        0x01

/* CTRL_REG3 */
#define REG3_I1_INT1    0x80
#define REG3_I1_BOOT    0x40
#define REG3_H_LACTIVE  0x20
#define REG3_PP_OD      0x10
#define REG3_I2_DRDY    0x08
#define REG3_I2_WTM     0x04
#define REG3_I2_ORUN    0x02
#define REG3_I2_EMPTY   0x01

/* CTRL_REG4 */
#define REG4_BDU        0x80
#define REG4_BLE        0x40
#define REG4_FS1        0x20
#define REG4_FS0        0x10
//#define REG4_RESERVED   0x08
#define REG4_ST1        0x04
#define REG4_ST0        0x02
#define REG4_SIM        0x01

/* INT1_CFG */
#define INT1_ANDOR      0x80
#define INT1_LIR        0x40
#define INT1_ZHIE       0x20
#define INT1_ZLIE       0x10
#define INT1_YHIE       0x08
#define INT1_YLIE       0x04
#define INT1_XHIE       0x02
#define INT1_XLIE       0x01


/* Access types */
#define WRITE           0x00
#define MULTI_WRITE     0x40
#define READ            0x80
#define MULTI_READ      0xC0


/* Slave select (SS) signal */
#define SS_DIR  GPIO_P3_1_DIR
#define SS_PIN  GPIO_P3_1_PIN


static void StartFrame() {
    SS_PIN = GPIO_LOW;
}


static void StopFrame() {
    SS_PIN = GPIO_HIGH;
}


static void WriteRegistre( unsigned char adresse, unsigned char value)  {

    SPI_WriteRead(adresse);
    SPI_WriteRead(value);
}


static unsigned int ReadRegistre(unsigned char adresse)  {

    SPI_WriteRead( READ | adresse);

    return SPI_WriteRead(0);
}


static void ReadMultiRegistres(unsigned char adresse, unsigned int size, unsigned int data[])  {

    unsigned int i;

    SPI_WriteRead(  MULTI_READ | adresse);

    for (i = 0; i < size; i++) {
        data[i] = SPI_WriteRead(0);
    }
}


static unsigned int decode( unsigned int v0, unsigned int v1) {

    return ((v0 & 0xFF) | ((v1 & 0xFF) << 8));
}


void DriverPmodgyro_Init() {

    SPI_Start( SPI_MODE_3, SPI_SDI_SAMPLE_MIDDLE_SDO, SPI_8_BITS, SPI_P3);

    SS_DIR = GPIO_OUTPUT;
    StopFrame();

    StartFrame();
    WriteRegistre( CTRL_REG3, 0);
    StopFrame();
    StartFrame();
    WriteRegistre( CTRL_REG4, REG4_FS1 | REG4_FS0);
    StopFrame();
    StartFrame();
    WriteRegistre( CTRL_REG1, REG1_PD | REG1_ZEN | REG1_YEN | REG1_XEN);
    StopFrame();
}


int DriverPmodgyro_GetZ() {

    unsigned int temp[2] = {0, 0};
    unsigned int zAxis = 0;

    StartFrame();
    ReadMultiRegistres(OUT_Z_L, 2, temp);
    StopFrame();

    zAxis = temp[0];
    zAxis |= (temp[1] << 8);
    zAxis = decode(temp[0], temp[1]);

    return (int)zAxis;
}


unsigned int DriverPmodgyro_GetDeviceID() {

    unsigned int DeviceID;

    StartFrame();
    DeviceID = ReadRegistre(WHO_AM_I);
    StopFrame();

    return DeviceID;
}
