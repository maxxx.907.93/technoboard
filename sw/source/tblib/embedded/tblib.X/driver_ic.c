/* The MIT License (MIT)

Copyright (c) 2015 Jean-Sebastien Castonguay

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

#include "technoboard.h"
#include "system.h"
#include <limits.h>


typedef struct {
    uint32_t lastEventTime;
    uint32_t secondLastEventTime;
    bool EventTimeValid;
    bool EventDeltaTimeValid;
    bool NewEventTimeAvailable;
    bool NewEventDeltaTimeAvailable;
    void (*EventHandler)();
} ICInfo;

static ICInfo icInfo[IC_NB_OF_MODULES] = {{.EventHandler = NULL}, {.EventHandler = NULL}};


bool IC_InstallEventHandler( IC_Module module, void (*handler)()) {

    bool result;

    if (module < IC_NB_OF_MODULES) {
        icInfo[module].EventHandler = handler;
        result = true;
    } else {
        result = false;
    }

    return result;
}


bool IC_Start( IC_Module module, IC_Input input, IC_EventType type) {

    bool result;

    if (module < IC_NB_OF_MODULES) {

        result = true;
        
        icInfo[module].EventTimeValid = false;
        icInfo[module].EventDeltaTimeValid = false;
        icInfo[module].NewEventTimeAvailable = false;
        icInfo[module].NewEventDeltaTimeAvailable = false;

        switch (module) {
            case IC_MODULE_1:
            default:
                iPPSInput( IN_FN_PPS_IC1, input);
                ConfigIntCapture12( IC_INT_ON | IC_INT_PRIOR_3);

                IC1CON1bits.ICSIDL  = 0;       // continue at idle CPU
                IC1CON1bits.ICI     = 0;       // interrupt every capture event
                IC1CON1bits.ICM     = type;
                IC1CON2bits.IC32    = 1;       // 32 bit mode only
                IC1CON1bits.ICTSEL  = 0b111;   // select Fcy as clock source
                IC1CON2bits.ICTRIG  = 0;       // Synchronize IC Mode
                IC1CON2bits.SYNCSEL = 0b00000; // Select No Sync Source

                IC2CON1bits.ICSIDL  = 0;       // continue at idle CPU
                IC2CON1bits.ICI     = 0;       // interrupt every capture event
                IC2CON1bits.ICM     = type;
                IC2CON2bits.IC32    = 1;       // 32 bit mode only
                IC2CON1bits.ICTSEL  = 0b111;   // select Fcy as clock source
                IC2CON2bits.ICTRIG  = 0;       // Synchronize IC Mode
                IC2CON2bits.SYNCSEL = 0b00000; // Select No Sync Source

                break;
            case IC_MODULE_2:
                iPPSInput( IN_FN_PPS_IC3, input);
                ConfigIntCapture34( IC_INT_ON | IC_INT_PRIOR_3);

                IC3CON1bits.ICSIDL  = 0;       // continue at idle CPU
                IC3CON1bits.ICI     = 0;       // interrupt every capture event
                IC3CON1bits.ICM     = type;
                IC3CON2bits.IC32    = 1;       // 32 bit mode only
                IC3CON1bits.ICTSEL  = 0b111;   // select Fcy as clock source
                IC3CON2bits.ICTRIG  = 0;       // Synchronize IC Mode
                IC3CON2bits.SYNCSEL = 0b00000; // Select No Sync Source

                IC4CON1bits.ICSIDL  = 0;       // continue at idle CPU
                IC4CON1bits.ICI     = 0;       // interrupt every capture event
                IC4CON1bits.ICM     = type;
                IC4CON2bits.IC32    = 1;       // 32 bit mode only
                IC4CON1bits.ICTSEL  = 0b111;   // select Fcy as clock source
                IC4CON2bits.ICTRIG  = 0;       // Synchronize IC Mode
                IC4CON2bits.SYNCSEL = 0b00000; // Select No Sync Source
                break;
        }
    } else {
        result = false;
    }

    return result;
}


uint32_t IC_GetLastEventTime( IC_Module module) {

    uint32_t lastEventTime = 0;

    if (module < IC_NB_OF_MODULES) {
        INTERRUPT_PROTECT(lastEventTime = icInfo[module].lastEventTime);
        icInfo[module].NewEventTimeAvailable = false;
    }
    
    return lastEventTime;
}


uint32_t IC_GetEventsDeltaTime( IC_Module module) {

    uint32_t delta = 0;
    
    if (module < IC_NB_OF_MODULES) {
        
        uint32_t lastEventTime;
        uint32_t secondLastEventTime;
    
        INTERRUPT_PROTECT(lastEventTime = icInfo[module].lastEventTime; secondLastEventTime = icInfo[module].secondLastEventTime);
        icInfo[module].NewEventDeltaTimeAvailable = false;

        if (lastEventTime >= secondLastEventTime) {
            delta = lastEventTime - secondLastEventTime;
        } else {
            delta = ULONG_MAX - (secondLastEventTime - lastEventTime) + 1;
        }    
    }

    return delta;
}


bool IC_Stop( IC_Module module) {

    bool result = true;

    switch (module) {
        case IC_MODULE_1:
            IC1CON1bits.ICM = 0b000;
            IC2CON1bits.ICM = 0b000;
            break;
        case IC_MODULE_2:
            IC3CON1bits.ICM = 0b000;
            IC4CON1bits.ICM = 0b000;
            break;
        default:
            result = false;
            break;
    }

    if (result) {
        icInfo[module].EventHandler = NULL;
    }

    return result;
}


bool IC_IsEventTimeValid( IC_Module module) {

    bool valid;

    if (module < IC_NB_OF_MODULES) {
        valid = icInfo[module].EventTimeValid;
    } else {
        valid = false;
    }

    return valid;
}


bool IC_IsEventDeltaTimeValid( IC_Module module) {
    
    bool valid;

    if (module < IC_NB_OF_MODULES) {
        valid = icInfo[module].EventDeltaTimeValid;
    } else {
        valid = false;
    }

    return valid;
}


bool IC_NewEventTimeAvailable(IC_Module module) {

    bool available;

    if (module < IC_NB_OF_MODULES) {
        available = icInfo[module].NewEventTimeAvailable;
    } else {
        available = false;
    }

    return available;
}


bool IC_NewEventDeltaTimeAvailable(IC_Module module) {

    bool available;

    if (module < IC_NB_OF_MODULES) {
        available = icInfo[module].NewEventDeltaTimeAvailable;
    } else {
        available = false;
    }

    return available;
}


static void ManageInterrupt( IC_Module module) {
    
    uint32_t LSWord;
    uint32_t MSWord;
    
    icInfo[module].secondLastEventTime = icInfo[module].lastEventTime;
    
    switch (module) {
        case IC_MODULE_1:
        default:
            LSWord = IC1BUF;
            MSWord = IC2BUF;
            MSWord <<= 16;
            icInfo[module].lastEventTime = LSWord | MSWord;
            break;
        case IC_MODULE_2:
            LSWord = IC3BUF;
            MSWord = IC4BUF;
            MSWord <<= 16;
            icInfo[module].lastEventTime = LSWord | MSWord;
            break;
    }
    
    if (icInfo[module].EventTimeValid) {
        icInfo[module].EventDeltaTimeValid = true;
        icInfo[module].NewEventDeltaTimeAvailable = true;
    }
    icInfo[module].EventTimeValid = true;
    icInfo[module].NewEventTimeAvailable = true;

    if (icInfo[module].EventHandler != NULL) {
        icInfo[module].EventHandler();
    }
}


void __attribute__((interrupt,no_auto_psv)) _IC12Interrupt(void) {
    ManageInterrupt(IC_MODULE_1);
    IC12_Clear_Intr_Status_Bit;
}


void __attribute__((interrupt,no_auto_psv)) _IC34Interrupt(void) {
    ManageInterrupt(IC_MODULE_2);
    IC34_Clear_Intr_Status_Bit;
}
