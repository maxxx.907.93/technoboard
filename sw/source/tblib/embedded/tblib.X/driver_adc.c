/* The MIT License (MIT)

Copyright (c) 2015 Jean-Sebastien Castonguay

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

#include "driver_adc.h"
#include "technoboard.h"
#include "system.h"


static unsigned int rawValues[ADC_NB_OF_CHANNEL];
static bool dataValid = false;


void ADC_Start() {

    unsigned int i;

    for (i = 0; i < ADC_NB_OF_CHANNEL; i++) {
        rawValues[i] = 0;
    }
    dataValid = false;

    unsigned int config1 =
          ADC_IDLE_STOP
        | ADC_FORMAT_INTG

        /* Auto-acquisition et auto-conversion: le tout se fait automatiquement
         * en continue sans intervention. */
        | ADC_CLK_AUTO
        | ADC_AUTO_SAMPLING_ON;

    unsigned int config2 =
          ADC_VREF_AVDD_AVSS

        /* Toutes les entr�es sont scann�es automatiquement avant de g�n�rer
         * une interruption. Donc le nombre de lectures avant interrupt doit
         * �tre PILOTE_NB_OF_AN. */
        | ADC_SCAN_ON
        | ADC_INTR_2_CONV

        /* Utilisation de la strat�gie "Using Dual, 8-Word Buffers". De cette
         * fa�on, il n'y aura pas de overrun si le ISR prend du retard.
         * Note: PILOTE_NB_OF_AN ne doit pas d�passer 8 pour utiliser cette
         * strat�gie. */
        | ADC_ALT_BUF_ON

        | ADC_ALT_INPUT_OFF;

    unsigned int config3 =
          ADC_CONV_CLK_SYSTEM
        | ADC_SAMPLE_TIME_31
        | ADC_CONV_CLK_64Tcy;

    unsigned int configport = ENABLE_AN0_DIG | ENABLE_AN1_DIG | ENABLE_AN2_DIG  | ENABLE_AN3_DIG | 
                              ENABLE_AN4_DIG | ENABLE_AN4_DIG | ENABLE_AN4_DIG  | ENABLE_AN4_DIG |
                              ENABLE_AN4_DIG | ENABLE_AN9_ANA | ENABLE_AN10_ANA | ENABLE_AN11_DIG;

    unsigned int configscan = ADC_SCAN_AN9 | ADC_SCAN_AN10;

    CloseADC10();
    SetChanADC10( ADC_CH0_NEG_SAMPLEA_VREFN);
    OpenADC10( config1, config2, config3, configport, configscan);
    SetPriorityIntADC1(3);
    ADC1_Clear_Intr_Status_Bit;
    EnableIntADC1;
    EnableADC1;
    Timer_Wait(1);
}


void ADC_Stop() {
    DisableIntADC1;
    DisableADC1;
    CloseADC10();
    DriverADC_SetAllPinsInDigitalMode();
}


/* According to "Using Dual, 8-Word Buffers" strategy. */
static inline unsigned char GetADCxBUFRangeToRead() {

    unsigned char StartingADCxBUF;

    if (AD1CON2bits.BUFS == 1) {
        StartingADCxBUF = 0;
    } else {
        StartingADCxBUF = 8;
    }

    return StartingADCxBUF;
}


void __attribute__ ((__interrupt__,auto_psv)) _ADC1Interrupt(void)
{
    unsigned char NoADCxBUFtoRead;
    unsigned int  i;

    NoADCxBUFtoRead = GetADCxBUFRangeToRead();
    for (i = 0; i < ADC_NB_OF_CHANNEL; i++, NoADCxBUFtoRead++) {
        rawValues[i] = ReadADC10( NoADCxBUFtoRead);
    }

    ADC1_Clear_Intr_Status_Bit;
    dataValid = true;
}


int ADC_GetRawValue(ADC_Channel channel) {

    int valueCopy;

    if ((channel < ADC_NB_OF_CHANNEL) && (dataValid)) {
        INTERRUPT_PROTECT( valueCopy = rawValues[channel]);
    } else {
        valueCopy = -1;
    }

    return valueCopy;
}

bool ADC_IsDataValid() {
    return dataValid;
}
