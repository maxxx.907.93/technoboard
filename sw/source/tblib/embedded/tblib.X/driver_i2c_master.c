/* The MIT License (MIT)

Copyright (c) 2015 Jean-Sebastien Castonguay

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

#include "technoboard.h"
#include <stdint.h>
#include <stdbool.h>
#include <i2c.h>

#define ACK   0
#define NACK  1

static I2C_Error error = I2C_NO_ERROR;

static uint8_t currentSlaveAddress = 0;

void I2C_Start( I2C_ClockRate ClockRate) {
    OpenI2C1( I2C_ON | I2C_7BIT_ADD, ClockRate);
    IdleI2C1();
}

void I2C_Stop() {
    currentSlaveAddress = 0;
    StopI2C1();
    IdleI2C1();
}

void I2C_SendStart( uint8_t slaveAddress, I2C_AccessType accessType) {

    currentSlaveAddress = slaveAddress;
    error = I2C_NO_ERROR;

    if (accessType >= I2C_NB_OF_ACCESS_TYPE) {
        error = I2C_PARAMETER_ERROR;
    } else {
        StartI2C1();
        IdleI2C1();
        MasterWriteI2C1( (currentSlaveAddress << 1) | accessType);
        IdleI2C1();

        if (I2C1STATbits.ACKSTAT == NACK) {
            error = I2C_NO_ACK_RECEIVED_1;
        }
    }
}

void I2C_SendRestart( I2C_AccessType accessType) {
    
    if (error == I2C_NO_ERROR) {
        if (accessType >= I2C_NB_OF_ACCESS_TYPE) {
            error = I2C_PARAMETER_ERROR;
        } else {
            RestartI2C1();
            IdleI2C1();
            MasterWriteI2C1( (currentSlaveAddress << 1) | accessType);
            IdleI2C1();

            if (I2C1STATbits.ACKSTAT == NACK) {
                error = I2C_NO_ACK_RECEIVED_2;
            }
        }
    }
}

void I2C_SendStop() {
    StopI2C1();
    IdleI2C1();
}

void I2C_WriteData( uint8_t * data, unsigned int size) {

    if (error == I2C_NO_ERROR) {
        if (data != NULL) {

            unsigned int i = 0;
            
            while ((i < size) && (error == I2C_NO_ERROR)) {
                MasterWriteI2C1( data[i]);
                i++;
                IdleI2C1();

                if (I2C1STATbits.ACKSTAT == NACK) {
                    error = I2C_NO_ACK_RECEIVED_3;
                }
            }
        } else {
            error = I2C_PARAMETER_ERROR;
        }
    }
}

void I2C_Write( uint8_t data) {
    I2C_WriteData(&data, 1);
}

void I2C_ReadData( uint8_t * data, unsigned int size) {

    if (error == I2C_NO_ERROR) {
        if (data != NULL) {
            unsigned int status = MastergetsI2C1( size, data, 10000);

            if (status != 0) {
                error = I2C_READING_ERROR;
            }
        } else {
            error = I2C_PARAMETER_ERROR;
        }
    }
}

uint8_t I2C_Read() {

    uint8_t data;

    I2C_ReadData( &data, 1);

    return data;
}

I2C_Error I2C_GetStatus() {
    return error;
}