/* The MIT License (MIT)

Copyright (c) 2015 Jean-Sebastien Castonguay

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

#include "technoboard.h"

static bool SPIStarted = false;

bool SPI_Start( SPI_Mode mode, SPI_SampleTime sampleTime, SPI_SizeOfData size, SPI_Output output) {

    bool result = true;
    unsigned int config1 = 0;
    unsigned int config2 = 0;
    unsigned int config3 = 0;

    CloseSPI2();

    ConfigIntSPI2( SPI_INT_DIS | SPI_INT_PRI_4);

    config1 = ENABLE_SCK_PIN
            | ENABLE_SDO_PIN
            | MASTER_ENABLE_ON
            | SEC_PRESCAL_8_1
            | PRI_PRESCAL_64_1;

    switch (mode) {
        case SPI_MODE_0:
            config1 |= SPI_CKE_ON | CLK_POL_ACTIVE_HIGH;
            break;
        case SPI_MODE_1:
            config1 |= SPI_CKE_OFF | CLK_POL_ACTIVE_HIGH;
            break;
        case SPI_MODE_2:
            config1 |= SPI_CKE_ON | CLK_POL_ACTIVE_LOW;
            break;
        case SPI_MODE_3:
            config1 |= SPI_CKE_OFF | CLK_POL_ACTIVE_LOW;
            break;
        default:
            result = false;
            break;
    }

    switch (sampleTime) {
        case SPI_SDI_SAMPLE_MIDDLE_SDO:
            config1 |= SPI_SMP_OFF;
            break;
        case SPI_SDI_SAMPLE_END_SDO:
            config1 |= SPI_SMP_ON;
            break;
        default:
            result = false;
            break;
    }

    switch (size) {
        case SPI_8_BITS:
            config1 |= SPI_MODE8_ON;
            break;
        case SPI_16_BITS:
            config1 |= SPI_MODE16_ON;
            break;
        default:
            result = false;
            break;
    }

    switch (output) {
        case SPI_P3:
            PPSOutput( PPS_RP6, PPS_SDO2);
            PPSInput( PPS_SDI2, PPS_RP15);
            PPSOutput( PPS_RP14, PPS_SCK2OUT);
            break;
        case SPI_P4_2_TO_4:
            PPSOutput( PPS_RP8, PPS_SDO2);
            PPSInput( PPS_SDI2, PPS_RP9);
            PPSOutput( PPS_RP13, PPS_SCK2OUT);
            break;
        case SPI_P4_8_TO_10:
            PPSOutput( PPS_RP4, PPS_SDO2);
            PPSInput( PPS_SDI2, PPS_RP3);
            PPSOutput( PPS_RP2, PPS_SCK2OUT);
            break;
        default:
            result = false;
            break;
    }

    config2 = FRAME_ENABLE_OFF;
    config3 = SPI_ENABLE
            | SPI_IDLE_CON
            | SPI_RX_OVFLOW_CLR;

    if (result) {
        OpenSPI2( config1, config2, config3);
        SPIStarted = true;
    }
    
    return result;
}


unsigned int SPI_WriteRead( unsigned int data) {

    unsigned int dataRead = 0;

    if (SPIStarted) {
        WriteSPI2( data);
        while (SPI2_Tx_Buf_Full){;}
        while (!SPI2_Rx_Buf_Full){;}
        dataRead = ReadSPI2();
    }

    return dataRead;
}


void SPI_Stop() {
    SPIStarted = false;
    PPSOutput( PPS_RP8, PPS_NULL);
    PPSOutput( PPS_RP13, PPS_NULL);
    CloseSPI2();
}
