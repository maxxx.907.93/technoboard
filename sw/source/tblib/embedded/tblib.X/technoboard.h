/* The MIT License (MIT)

Copyright (c) 2015 Jean-Sebastien Castonguay

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

#ifndef TECHNOBOARD_H
#define	TECHNOBOARD_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <p24FJ64GB002.h>

#define USE_AND_OR /* To enable AND_OR mask setting */
#include <timer.h>
#include <adc.h>
#include <outcompare.h>
#include <PPS.h>
#include <spi.h>
#include <uart.h>
#include <incap.h>

#define TECHNOBOARD_LIB_VERSION  "1.3.0"

#define STR_FORMAT_INT     "%d"
#define STR_FORMAT_UINT    "%u"
#define STR_FORMAT_LONG    "%ld"
#define STR_FORMAT_ULONG   "%lu"
#define STR_FORMAT_FLOAT   "%f"
#define STR_FORMAT_HEX     "%X"
#define STR_FORMAT_CHAR    "%c"

#define MS_PAR_S  1000  // Unit: ms/s

#define MIN(x,y)  ((x) < (y) ? (x) : (y))
#define MAX(x,y)  ((x) > (y) ? (x) : (y))


/* Minimum technoboard functions
 * ============================= */
typedef enum {
    TECHNOBOARD_NO_FEATURES   = 0x00,
    TECHNOBOARD_USB           = 0x01,
    TECHNOBOARD_TIMER         = 0x02,
    TECHNOBOARD_ALL_FEATURES  = 0x03
} TechnoBoard_Features;

void TechnoBoard_Initialise( TechnoBoard_Features features);
void TechnoBoard_CyclicProcess();
char * TechnoBoard_GetVersion();


/* General Purpose Input/Output (GPIO)
 * =================================== */
#define GPIO_P3_1_DIR     TRISAbits.TRISA0
#define GPIO_P3_2_DIR     TRISAbits.TRISA1
#define GPIO_P3_3_DIR     TRISBbits.TRISB15
#define GPIO_P3_4_DIR     TRISBbits.TRISB14
#define GPIO_P4_1_DIR     TRISBbits.TRISB7
#define GPIO_P4_2_DIR     TRISBbits.TRISB8
#define GPIO_P4_3_DIR     TRISBbits.TRISB9
#define GPIO_P4_4_DIR     TRISBbits.TRISB13
#define GPIO_P4_7_DIR     TRISAbits.TRISA4
#define GPIO_P4_8_DIR     TRISBbits.TRISB4
#define GPIO_P4_9_DIR     TRISBbits.TRISB3
#define GPIO_P4_10_DIR    TRISBbits.TRISB2

#define GPIO_P3_1_READ    PORTAbits.RA0
#define GPIO_P3_2_READ    PORTAbits.RA1
#define GPIO_P3_3_READ    PORTBbits.RB15
#define GPIO_P3_4_READ    PORTBbits.RB14
#define GPIO_P4_1_READ    PORTBbits.RB7
#define GPIO_P4_2_READ    PORTBbits.RB8
#define GPIO_P4_3_READ    PORTBbits.RB9
#define GPIO_P4_4_READ    PORTBbits.RB13
#define GPIO_P4_7_READ    PORTAbits.RA4
#define GPIO_P4_8_READ    PORTBbits.RB4
#define GPIO_P4_9_READ    PORTBbits.RB3
#define GPIO_P4_10_READ   PORTBbits.RB2

#define GPIO_P3_1_WRITE   LATAbits.LATA0
#define GPIO_P3_2_WRITE   LATAbits.LATA1
#define GPIO_P3_3_WRITE   LATBbits.LATB15
#define GPIO_P3_4_WRITE   LATBbits.LATB14
#define GPIO_P4_1_WRITE   LATBbits.LATB7
#define GPIO_P4_2_WRITE   LATBbits.LATB8
#define GPIO_P4_3_WRITE   LATBbits.LATB9
#define GPIO_P4_4_WRITE   LATBbits.LATB13
#define GPIO_P4_7_WRITE   LATAbits.LATA4
#define GPIO_P4_8_WRITE   LATBbits.LATB4
#define GPIO_P4_9_WRITE   LATBbits.LATB3
#define GPIO_P4_10_WRITE  LATBbits.LATB2

typedef enum {
    GPIO_OUTPUT = 0,
    GPIO_INPUT = 1
} GPIO_Direction;

typedef enum {
    GPIO_LOW = 0,
    GPIO_HIGH = 1
} GPIO_PinState;


/* Multifonctions pin (RB5) */
#define GPIO_MULTIFUNCTION_DIR    TRISBbits.TRISB5
#define GPIO_MULTIFUNCTION_READ   PORTBbits.RB5
#define GPIO_MULTIFUNCTION_WRITE  LATBbits.LATB5

#define LED_Init()         GPIO_MULTIFUNCTION_DIR = GPIO_OUTPUT
#define LED_STATE          GPIO_MULTIFUNCTION_WRITE
#define LED_ToggleState()  LED_STATE ^= 1;

typedef enum {
    LED_ON = 0,
    LED_OFF = 1
} LED_State;

#define Button_Init()  GPIO_MULTIFUNCTION_DIR = GPIO_INPUT
#define BUTTON_STATE   GPIO_MULTIFUNCTION_READ

typedef enum {
    BUTTON_PRESSED = 0,
    BUTTON_RELEASED = 1
} Button_State;


/* Serial Communication (USB)
 * ========================== */
unsigned int USB_WriteData( uint8_t * data, unsigned int size);
unsigned int USB_PrintString( char * string);
unsigned int USB_PrintInt( int value);
unsigned int USB_PrintUInt( unsigned int value);
unsigned int USB_PrintHex( unsigned int value);
unsigned int USB_ReadData( uint8_t * data, unsigned int size);
bool USB_IsDataReceived();
char USB_GetChar();
void USB_PutChar( char character);
int USB_GetInt();
unsigned int USB_GetUInt();
float USB_GetFloat();
char * USB_GetString();

#define STRING_NB_MAX  16
#define USB_PrintNumber( nb, format) do { \
        char stringNb[STRING_NB_MAX]; \
        snprintf( stringNb, STRING_NB_MAX, format, (nb)); \
        USB_PrintString( stringNb); \
    } while (0)


/* The time base timer definitions
 * =============================== */
typedef uint32_t Timer_ms;

Timer_ms Timer_GetTime();
Timer_ms Timer_GetElapseTime( Timer_ms referenceTime);
void Timer_Wait( Timer_ms delay);


/* The Analog to Digital Converter (ADC)
 * ===================================== */
#define ADC_MAX_RAW_VALUE  1023

typedef enum {
    ADC_P2_3,
    ADC_P2_4,

    ADC_NB_OF_CHANNEL
} ADC_Channel;

void ADC_Start();
int ADC_GetRawValue(ADC_Channel channel);
bool ADC_IsDataValid();
void ADC_Stop();


/* Pulse Width Modulation (PWM)
 * ============================ */
typedef enum {
    PWM_OUTPUT_P4_2,
    PWM_OUTPUT_P4_8,

    PWM_NB_OF_OUTPUT
} PWM_Output;

typedef enum {
    PWM_TIMER_AT_63_ns,
    PWM_TIMER_AT_500_ns,
    PWM_TIMER_AT_4_us,
    PWM_TIMER_AT_16_us,

    PWM_TIMER_NB_OF_PERIOD
} PWM_TimerSourcePeriod;

bool PWM_Start(
        PWM_Output output,
        PWM_TimerSourcePeriod timerScrPeriod,
        unsigned int pwmPeriod,
        unsigned int dutyCycle);
bool PWM_SetDutyCycle( PWM_Output output, unsigned int dutyCycle);
bool PWM_Stop( PWM_Output output);


/* Digital to Analog Converter (DAC)
 * ================================= */
#define DAC_MAX_RAW_VALUE  4095

typedef enum {
    DAC_P2_1,
    DAC_P2_2,

    DAC_NB_OF_OUTPUT
} DAC_Output;

void DAC_Start();
bool DAC_SetRawValue(DAC_Output output, unsigned int rawValue);
void DAC_Stop();


/* Inter-Integrated Circuit (I2C)
 * ============================== */
typedef enum {
    I2C_100KHZ = 0x9D,
    I2C_400KHZ = 0x25,
    I2C_1MHZ   = 0x0D
} I2C_ClockRate;

typedef enum {
    I2C_NO_ERROR,
    I2C_NO_ACK_RECEIVED_1,
    I2C_NO_ACK_RECEIVED_2,
    I2C_NO_ACK_RECEIVED_3,
    I2C_READING_ERROR,
    I2C_PARAMETER_ERROR
} I2C_Error;

typedef enum {
    I2C_WRITE = 0,
    I2C_READ  = 1,

    I2C_NB_OF_ACCESS_TYPE
} I2C_AccessType;

void I2C_Start( I2C_ClockRate ClockRate);
void I2C_SendStart( uint8_t slaveAddress, I2C_AccessType accessType);
void I2C_SendRestart( I2C_AccessType accessType);
void I2C_SendStop();
void I2C_WriteData( uint8_t * data, unsigned int size);
void I2C_Write( uint8_t data);
void I2C_ReadData( uint8_t * data, unsigned int size);
uint8_t I2C_Read();
I2C_Error I2C_GetStatus();
void I2C_Stop();


/* Serial Peripheral Interface (SPI)
 * ================================= */
typedef enum {
    SPI_MODE_0,  // SCK idle low and SDO change from active (high) to idle (low)
    SPI_MODE_1,  // SCK idle low and SDO change from idle (low) to active (high)
    SPI_MODE_2,  // SCK idle high and SDO change from active (low) to idle (high)
    SPI_MODE_3   // SCK idle high and SDO change from idle (high) to active (low)
} SPI_Mode;

typedef enum {
    SPI_SDI_SAMPLE_MIDDLE_SDO,
    SPI_SDI_SAMPLE_END_SDO
} SPI_SampleTime;

typedef enum {
    SPI_8_BITS,
    SPI_16_BITS
} SPI_SizeOfData;

typedef enum {
    SPI_P3,
    SPI_P4_2_TO_4,
    SPI_P4_8_TO_10
} SPI_Output;

bool SPI_Start( SPI_Mode mode, SPI_SampleTime sampleTime, SPI_SizeOfData size, SPI_Output output);
unsigned int SPI_WriteRead( unsigned int data);
void SPI_Stop();


/* Universal Asynchronous Receiver/Transmitter (UART)
 * ================================================== */
typedef enum {
    UART_9600   = 416,
    UART_19200  = 207,
    UART_115200 = 34
} UART_Baudrate;

typedef enum {
    UART_TXP3_2_RXP3_3,
    UART_TXP4_2_RXP4_3,
    UART_TXP4_8_RXP4_9
} UART_Port;

bool UART_Start( UART_Baudrate baudrate, UART_Port port);
unsigned int UART_WriteData( uint8_t * data, unsigned int sizeToWrite);
unsigned int UART_PrintString( char * string);
unsigned int UART_PrintInt( int value);
unsigned int UART_PrintUInt( unsigned int value);
unsigned int UART_PrintHex( unsigned int value);
unsigned int UART_ReadData( uint8_t * data, unsigned int sizeAvailable);
bool UART_IsDataReceived();
char UART_GetChar();
void UART_PutChar( char character);
void UART_Stop();

#define STRING_NB_MAX  16
#define UART_PrintNumber( nb, format) do { \
        char stringNb[STRING_NB_MAX]; \
        snprintf( stringNb, STRING_NB_MAX, format, (nb)); \
        UART_PrintString(stringNb); \
    } while (0)


/* Input Capture (IC)
 * ================== */
#define IC_RESULUTION_IN_S  625e-10

typedef enum {
    IC_MODULE_1,
    IC_MODULE_2,

    IC_NB_OF_MODULES
} IC_Module;

typedef enum {
    IC_INPUT_P4_3  = IN_PIN_PPS_RP9,
    IC_INPUT_P4_4  = IN_PIN_PPS_RP13,
    IC_INPUT_P4_9  = IN_PIN_PPS_RP3,
    IC_INPUT_P4_10 = IN_PIN_PPS_RP2
} IC_Input;

typedef enum {
    IC_FALLING_AND_RISING_EDGES = 1,
    IC_FALLING_EDGES            = 2,
    IC_RISING_EDGES             = 3
} IC_EventType;

bool IC_InstallEventHandler( IC_Module module, void (*handler)());
bool IC_Start( IC_Module module, IC_Input input, IC_EventType type);
uint32_t IC_GetLastEventTime( IC_Module module);
uint32_t IC_GetEventsDeltaTime( IC_Module module);
bool IC_IsEventTimeValid( IC_Module module);
bool IC_IsEventDeltaTimeValid( IC_Module module);
bool IC_NewEventTimeAvailable(IC_Module module);
bool IC_NewEventDeltaTimeAvailable(IC_Module module);
bool IC_Stop( IC_Module module);


#ifdef	__cplusplus
}
#endif

#endif
