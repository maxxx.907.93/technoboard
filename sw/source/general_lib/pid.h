/* The MIT License (MIT)

Copyright (c) 2015 Jean-Sebastien Castonguay

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

/* This source code is derived from the TIM WESCOTT's PID Without
a PhD. See http://m.eet.com/media/1112634/f-wescot.pdf */

#ifndef PID_H
#define	PID_H

#include <stdint.h>

#define PID_MULTI_FACTOR_SHIFT  10  // Bit shifting

typedef int32_t Gain;  // Unit: 1/1024 unit/digit.

typedef struct {
    int32_t dState; // Last position input
    int32_t iState; // Integrator state
    int32_t iMax;   // Maximum and minimum allowable integrator state
    int32_t iMin;
    Gain kp;        // Proportional gain
    Gain ki;        // Integral gain with
    Gain kd;        // Derivative gain
} Pid;


void Pid_Init( Pid * pid, Gain kp, Gain ki, Gain kd, int32_t iMax, int32_t iMin);
int32_t Pid_Update( Pid * pid, int32_t command, int32_t feedback);


#ifdef	__cplusplus
}
#endif

#endif