/* The MIT License (MIT)

Copyright (c) 2015 Jean-Sebastien Castonguay

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

#include "console.h"
#include "technoboard.h"
#include "version.h"
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>


#define COMMAND_PROMPT   "cmd>"
#define FIELD_SEPARATOR  ","
#define END_OF_COMMAND   '\r'
#define END_OF_LINE      "\n\r"

#define CMD_HELP         "help"
#define CMD_VERSION      "version"
#define CMD_GET_ADC      "getadc"
#define CMD_CONF_GPIO    "confgpio"
#define CMD_SET_GPIO     "setgpio"
#define CMD_GET_GPIO     "getgpio"
#define CMD_START_PWM    "startpwm"
#define CMD_PWM_DC       "pwmdc"
#define CMD_STOP_PWM     "stoppwm"
#define CMD_START_DAC    "startdac"
#define CMD_SET_DAC      "setdac"
#define CMD_STOP_DAC     "stopdac"
#define CMD_SET_LED      "setled"
#define CDM_GET_BUTTON   "getbutton"

#define ERROR_EXPECTED_PARAMETER     "Parameter expected"
#define ERROR_NO_PARAMETER_EXPECTED  "No parameter expected"
#define ERROR_WRONG_PARAMETER        "Wrong parameter"

#define COMMAND_BUFFER_SIZE  80

typedef struct {
    char buffer[COMMAND_BUFFER_SIZE];
    unsigned int Index;
} CommandBuffer;

static CommandBuffer commandBuffer = {
    .Index = 0
};

static Timer_ms ADCProcessPeriod = 0;


static bool AddChar(CommandBuffer * const buffer, char c) {

    bool charAdded;

    if (buffer->Index < COMMAND_BUFFER_SIZE) {
        buffer->buffer[buffer->Index] = c;
        buffer->Index++;
        charAdded = true;
    } else {
        charAdded = false;
    }

    return charAdded;
}


static void Empty(CommandBuffer * const buffer) {
    buffer->Index = 0;
}


static void SendError( char * const errorMessage) {
    USB_PrintString("Error: ");
    USB_PrintString(errorMessage);
}


static bool GetCommand(CommandBuffer * const buffer) {

    bool commandReceived = false;
    
    while (USB_IsDataReceived()) {

        char c = USB_GetChar();

        if (c == END_OF_COMMAND) {
            commandReceived = true;
            c = '\0';  // Used to make the command a NULL terminated string.
        }

        if (AddChar(buffer, c) == false) {
            SendError("Command line too long: 80 char max");
            USB_PrintString(COMMAND_PROMPT);
            Empty(buffer);
        }
    }

    return commandReceived;
}


static void ProcessVersionCommand() {
    USB_PrintString("version,");
    USB_PrintString("tblib:");
    USB_PrintString(TechnoBoard_GetVersion());
    USB_PrintString(",header:" TECHNOBOARD_LIB_VERSION);
    USB_PrintString(",daqc:");
    USB_PrintString(APPLICATION_VERSION);
}


static void ProcessHelpCommand() {

    char * parameter = strtok(NULL, FIELD_SEPARATOR);

    if (parameter == NULL) {
        USB_PrintString(END_OF_LINE);
        USB_PrintString("technoboard application" END_OF_LINE);
        USB_PrintString("=======================" END_OF_LINE);
        ProcessVersionCommand();
        USB_PrintString("Here are the available commands:" END_OF_LINE);
        USB_PrintString("  help:       Display this information" END_OF_LINE);
        USB_PrintString("  version:    Display the technoboard lib and application versions");
    } else {
        SendError(ERROR_NO_PARAMETER_EXPECTED);
    }
}


static void PrintADCValues() {
    USB_PrintString("adc,");
    USB_PrintInt( ADC_GetRawValue(ADC_P2_3));
    USB_PrintString(FIELD_SEPARATOR);
    USB_PrintInt( ADC_GetRawValue(ADC_P2_4));
}


static void ADCProcess() {

    static Timer_ms timeout = 0;

    if ((ADCProcessPeriod > 0) && (ADC_IsDataValid() == true)) {
        if (Timer_GetElapseTime(timeout) > ADCProcessPeriod) {
            timeout = Timer_GetTime();
            PrintADCValues();
            USB_PrintString(END_OF_LINE);
        }
    }
}


static void ProcessGetAdcCommand() {

    char * period = strtok(NULL, FIELD_SEPARATOR);

    if (period != NULL) {
        ADCProcessPeriod = atoi(period);
        if (ADCProcessPeriod > 0) {
            ADC_Start();
        } else {
            ADC_Stop();
        }
    } else {
        ADC_Start();
        while (ADC_IsDataValid() == false) {;}
        PrintADCValues();
        ADC_Stop();
    }
}


static void ProcessConfGPIOCommand() {

    char * gpio = strtok(NULL, FIELD_SEPARATOR);
    char * directionStr = strtok(NULL, FIELD_SEPARATOR);

    if ((gpio != NULL) && (directionStr != NULL)) {

        GPIO_Direction direction;
        bool directionFound = true;

        if (directionStr[0] == 'i') {
            direction = GPIO_INPUT;
        } else if (directionStr[0] == 'o') {
            direction = GPIO_OUTPUT;
        } else {
            SendError(ERROR_WRONG_PARAMETER);
            directionFound = false;
        }

        if (directionFound) {
            if (strcmp(gpio, "3.1") == 0) {
                GPIO_P3_1_DIR = direction;
            } else if (strcmp(gpio, "3.2") == 0) {
                GPIO_P3_2_DIR = direction;
            } else if (strcmp(gpio, "3.3") == 0) {
                GPIO_P3_3_DIR = direction;
            } else if (strcmp(gpio, "3.4") == 0) {
                GPIO_P3_4_DIR = direction;
            } else if (strcmp(gpio, "4.1") == 0) {
                GPIO_P4_1_DIR = direction;
            } else if (strcmp(gpio, "4.2") == 0) {
                GPIO_P4_2_DIR = direction;
            } else if (strcmp(gpio, "4.3") == 0) {
                GPIO_P4_3_DIR = direction;
            } else if (strcmp(gpio, "4.4") == 0) {
                GPIO_P4_4_DIR = direction;
            } else if (strcmp(gpio, "4.7") == 0) {
                GPIO_P4_7_DIR = direction;
            } else if (strcmp(gpio, "4.8") == 0) {
                GPIO_P4_8_DIR = direction;
            } else if (strcmp(gpio, "4.9") == 0) {
                GPIO_P4_9_DIR = direction;
            } else if (strcmp(gpio, "4.10") == 0) {
                GPIO_P4_10_DIR = direction;
            } else {
                SendError(ERROR_WRONG_PARAMETER);
            }
        }
    } else {
        USB_PrintString(ERROR_EXPECTED_PARAMETER);
    }
}


static void ProcessSetGPIOCommand() {

    char * gpio = strtok(NULL, FIELD_SEPARATOR);
    char * levelStr = strtok(NULL, FIELD_SEPARATOR);

    if ((gpio != NULL) && (levelStr != NULL)) {

        unsigned int level = atoi(levelStr);
        
        if (strcmp(gpio, "3.1") == 0) {
            GPIO_P3_1_PIN = level;
        } else if (strcmp(gpio, "3.2") == 0) {
            GPIO_P3_2_PIN = level;
        } else if (strcmp(gpio, "3.3") == 0) {
            GPIO_P3_3_PIN = level;
        } else if (strcmp(gpio, "3.4") == 0) {
            GPIO_P3_4_PIN = level;
        } else if (strcmp(gpio, "4.1") == 0) {
            GPIO_P4_1_PIN = level;
        } else if (strcmp(gpio, "4.2") == 0) {
            GPIO_P4_2_PIN = level;
        } else if (strcmp(gpio, "4.3") == 0) {
            GPIO_P4_3_PIN = level;
        } else if (strcmp(gpio, "4.4") == 0) {
            GPIO_P4_4_PIN = level;
        } else if (strcmp(gpio, "4.7") == 0) {
            GPIO_P4_7_PIN = level;
        } else if (strcmp(gpio, "4.8") == 0) {
            GPIO_P4_8_PIN = level;
        } else if (strcmp(gpio, "4.9") == 0) {
            GPIO_P4_9_PIN = level;
        } else if (strcmp(gpio, "4.10") == 0) {
            GPIO_P4_10_PIN = level;
        } else {
            SendError(ERROR_WRONG_PARAMETER);
        }

    } else {
        USB_PrintString(ERROR_EXPECTED_PARAMETER);
    }
}


static void ProcessGetGPIOCommand() {

    char * gpio = strtok(NULL, FIELD_SEPARATOR);

    if (gpio != NULL) {

        bool level;
        bool pinFound = true;

        if (strcmp(gpio, "3.1") == 0) {
            level = GPIO_P3_1_PIN;
        } else if (strcmp(gpio, "3.2") == 0) {
            level = GPIO_P3_2_PIN;
        } else if (strcmp(gpio, "3.3") == 0) {
            level = GPIO_P3_3_PIN;
        } else if (strcmp(gpio, "3.4") == 0) {
            level = GPIO_P3_4_PIN;
        } else if (strcmp(gpio, "4.1") == 0) {
            level = GPIO_P4_1_PIN;
        } else if (strcmp(gpio, "4.2") == 0) {
            level = GPIO_P4_2_PIN;
        } else if (strcmp(gpio, "4.3") == 0) {
            level = GPIO_P4_3_PIN;
        } else if (strcmp(gpio, "4.4") == 0) {
            level = GPIO_P4_4_PIN;
        } else if (strcmp(gpio, "4.7") == 0) {
            level = GPIO_P4_7_PIN;
        } else if (strcmp(gpio, "4.8") == 0) {
            level = GPIO_P4_8_PIN;
        } else if (strcmp(gpio, "4.9") == 0) {
            level = GPIO_P4_9_PIN;
        } else if (strcmp(gpio, "4.10") == 0) {
            level = GPIO_P4_10_PIN;
        } else {
            SendError(ERROR_WRONG_PARAMETER);
            pinFound = false;
        }

        if (pinFound) {
            USB_PrintString("gpio,");
            USB_PrintInt(level);
        }

    } else {
        USB_PrintString(ERROR_EXPECTED_PARAMETER);
    }
}


static void ProcessStartPWMCommand() {
    
    char * outputStr = strtok(NULL, FIELD_SEPARATOR);
    char * sourcePeriodStr = strtok(NULL, FIELD_SEPARATOR);
    char * periodStr = strtok(NULL, FIELD_SEPARATOR);
    char * dutyCycleStr = strtok(NULL, FIELD_SEPARATOR);

    if ((outputStr != NULL) && (sourcePeriodStr != NULL) && (periodStr != NULL) && (dutyCycleStr != NULL)) {

        PWM_Output output = atoi(outputStr);
        PWM_TimerSourcePeriod timerScrPeriod = atoi(sourcePeriodStr);
        unsigned int pwmPeriod = atoi(periodStr);
        unsigned int dutyCycle = atoi(dutyCycleStr);

        if (PWM_Start( output, timerScrPeriod, pwmPeriod, dutyCycle) == false) {
            SendError(ERROR_WRONG_PARAMETER);
        }
    } else {
        SendError(ERROR_EXPECTED_PARAMETER);
    }
}


static void ProcessPWMDCCommand() {

    char * outputStr = strtok(NULL, FIELD_SEPARATOR);
    char * dutyCycleStr = strtok(NULL, FIELD_SEPARATOR);

    if ((outputStr != NULL) && (dutyCycleStr != NULL)) {

        PWM_Output output = atoi(outputStr);
        unsigned int dutyCycle = atoi(dutyCycleStr);

        if (PWM_SetDutyCycle( output, dutyCycle) == false) {
            SendError(ERROR_WRONG_PARAMETER);
        }
    } else {
        SendError(ERROR_EXPECTED_PARAMETER);
    }
}


static void ProcessStopPWMCommand() {

    char * outputStr = strtok(NULL, FIELD_SEPARATOR);

    if (outputStr != NULL) {

        PWM_Output output = atoi(outputStr);

        if (PWM_Stop( output) == false) {
            SendError(ERROR_WRONG_PARAMETER);
        }
    } else {
        SendError(ERROR_EXPECTED_PARAMETER);
    }
}


static void ProcessStartDACCommand() {

    char * parameter = strtok(NULL, FIELD_SEPARATOR);

    if (parameter == NULL) {
        DAC_Start();
    } else {
        SendError(ERROR_NO_PARAMETER_EXPECTED);
    }
}


static void ProcessSetDACCommand() {
    
    char * outputStr = strtok(NULL, FIELD_SEPARATOR);
    char * rawValueStr = strtok(NULL, FIELD_SEPARATOR);

    if ((outputStr != NULL) && (rawValueStr != NULL)) {

        DAC_Output output = atoi(outputStr);
        unsigned int rawValue = atoi(rawValueStr);

        if (DAC_SetRawValue( output, rawValue) == false) {
            SendError(ERROR_WRONG_PARAMETER);
        }
    } else {
        SendError(ERROR_EXPECTED_PARAMETER);
    }
}


static void ProcessStopDACCommand() {

    char * parameter = strtok(NULL, FIELD_SEPARATOR);

    if (parameter == NULL) {
        DAC_Stop();
    } else {
        SendError(ERROR_NO_PARAMETER_EXPECTED);
    }
}


void ProcessSetLed() {

    char * state = strtok(NULL, FIELD_SEPARATOR);

    if (state != NULL) {

        LED_Init();
        
        if (strcmp(state, "on") == 0) {
            LED_STATE = LED_ON;
        } else if (strcmp(state, "off") == 0) {
            LED_STATE = LED_OFF;
        } else if (strcmp(state, "toggle") == 0) {
            LED_ToggleState();
        } else {
            SendError(ERROR_WRONG_PARAMETER);
        }
    } else {
        USB_PrintString(ERROR_EXPECTED_PARAMETER);
    }
}


void ProcessGetButton() {

    char * state = strtok(NULL, FIELD_SEPARATOR);

    if (state == NULL) {

        Button_Init();
        Timer_Wait(1);

        if (BUTTON_STATE == BUTTON_PRESSED) {
            USB_PrintString("pressed");
        } else {
            USB_PrintString("released");
        }
    } else {
        USB_PrintString(ERROR_WRONG_PARAMETER);
    }
}


void Console_CyclicProcess() {

    if (GetCommand(&commandBuffer)) {
        if (commandBuffer.Index > 1) {

            char * commandName = strtok(commandBuffer.buffer, FIELD_SEPARATOR);

            if (strcmp( commandName, CMD_HELP) == 0) {
                ProcessHelpCommand();
            } else if (strcmp( commandName, CMD_VERSION) == 0) {
                ProcessVersionCommand();
            } else if (strcmp( commandName, CMD_GET_ADC) == 0) {
                ProcessGetAdcCommand();
            } else if (strcmp( commandName, CMD_CONF_GPIO) == 0) {
                ProcessConfGPIOCommand();
            } else if (strcmp( commandName, CMD_SET_GPIO) == 0) {
                ProcessSetGPIOCommand();
            } else if (strcmp( commandName, CMD_GET_GPIO) == 0) {
                ProcessGetGPIOCommand();
            } else if (strcmp( commandName, CMD_START_PWM) == 0) {
                ProcessStartPWMCommand();
            } else if (strcmp( commandName, CMD_PWM_DC) == 0) {
                ProcessPWMDCCommand();
            } else if (strcmp( commandName, CMD_STOP_PWM) == 0) {
                ProcessStopPWMCommand();
            } else if (strcmp( commandName, CMD_START_DAC) == 0) {
                ProcessStartDACCommand();
            } else if (strcmp( commandName, CMD_SET_DAC) == 0) {
                ProcessSetDACCommand();
            } else if (strcmp( commandName, CMD_STOP_DAC) == 0) {
                ProcessStopDACCommand();
            } else if (strcmp( commandName, CMD_SET_LED) == 0) {
                ProcessSetLed();
            } else if (strcmp( commandName, CDM_GET_BUTTON) == 0) {
                ProcessGetButton();
            } else {
                SendError("Unknown command");
            }
        }
        
        Empty(&commandBuffer);
        USB_PrintString(END_OF_LINE);
        USB_PrintString(COMMAND_PROMPT);
    }

    ADCProcess();
}
