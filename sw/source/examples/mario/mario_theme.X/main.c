/* The MIT License (MIT)

Copyright (c) 2015 Jean-Sebastien Castonguay

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

#include <stdio.h>
#include <stdlib.h>
#include "technoboard.h"

void Beep( float freq, Timer_ms duration) {

    unsigned int pwmPeriod;

    pwmPeriod = (1 / (freq * 0.0000005));
    
    PWM_Start( PWM_OUTPUT_P4_2, PWM_TIMER_AT_500_ns, pwmPeriod, 50);
    Timer_Wait(duration);
    PWM_Stop(PWM_OUTPUT_P4_2);
}

int main(int argc, char** argv) {

    TechnoBoard_Initialise(TECHNOBOARD_ALL_FEATURES);

    Beep(480,200);
    Beep(1568,200);
    Beep(1568,200);
    Beep(1568,200);

    Beep(739.99,200);
    Beep(783.99,200);
    Beep(783.99,200);
    Beep(783.99,200);

    Beep(369.99,200);
    Beep(392,200);
    Beep(369.99,200);
    Beep(392,200);
    Beep(392,400);
    Beep(196,400);

    Beep(739.99,200);
    Beep(783.99,200);
    Beep(783.99,200);
    Beep(739.99,200);
    Beep(783.99,200);
    Beep(783.99,200);
    Beep(739.99,200);
    Beep(83.99,200);
    Beep(880,200);
    Beep(830.61,200);
    Beep(880,200);
    Beep(987.77,400);

    Beep(880,200);
    Beep(783.99,200);
    Beep(698.46,200);
    Beep(739.99,200);
    Beep(783.99,200);
    Beep(783.99,200);
    Beep(739.99,200);
    Beep(783.99,200);
    Beep(783.99,200);
    Beep(739.99,200);
    Beep(783.99,200);
    Beep(880,200);
    Beep(830.61,200);
    Beep(880,200);
    Beep(987.77,400);

    Timer_Wait(200);

    Beep(1108,10);
    Beep(1174.7,200);
    Beep(1480,10);
    Beep(1568,200);

    Timer_Wait(200);

    Beep(739.99,200);
    Beep(783.99,200);
    Beep(783.99,200);
    Beep(739.99,200);
    Beep(783.99,200);
    Beep(783.99,200);
    Beep(739.99,200);
    Beep(783.99,200);
    Beep(880,200);
    Beep(830.61,200);
    Beep(880,200);
    Beep(987.77,400);

    Beep(880,200);
    Beep(783.99,200);
    Beep(698.46,200);

    Beep(659.25,200);
    Beep(698.46,200);
    Beep(784,200);
    Beep(880,400);
    Beep(784,200);
    Beep(698.46,200);
    Beep(659.25,200);

    Beep(587.33,200);
    Beep(659.25,200);
    Beep(698.46,200);
    Beep(784,400);
    Beep(698.46,200);
    Beep(659.25,200);
    Beep(587.33,200);

    Beep(523.25,200);
    Beep(587.33,200);
    Beep(659.25,200);
    Beep(698.46,400);
    Beep(659.25,200);
    Beep(587.33,200);
    Beep(493.88,200);
    Beep(523.25,200);

    Timer_Wait(400);

    Beep(349.23,400);
    Beep(392,200);
    Beep(329.63,200);
    Beep(523.25,200);
    Beep(493.88,200);
    Beep(466.16,200);

    Beep(440,200);
    Beep(493.88,200);
    Beep(523.25,200);
    Beep(880,200);
    Beep(493.88,200);
    Beep(880,200);
    Beep(1760,200);
    Beep(440,200);

    Beep(392,200);
    Beep(440,200);
    Beep(493.88,200);
    Beep(783.99,200);
    Beep(440, 200);
    Beep(783.99,200);
    Beep(1568,200);
    Beep(392,200);

    Beep(349.23,200);
    Beep(392,200);
    Beep(440,200);
    Beep(698.46,200);

    Beep(415.2,200);

    Beep(698.46,200);
    Beep(1396.92,200);
    Beep(349.23,200);

    Beep(329.63,200);
    Beep(311.13,200);
    Beep(329.63,200);
    Beep(659.25,200);
    Beep(698.46,400);
    Beep(783.99,400);

    Beep(440,200);
    Beep(493.88,200);
    Beep(523.25,200);
    Beep(880,200);
    Beep(493.88,200);
    Beep(880,200);
    Beep(1760,200);
    Beep(440,200);

    Beep(392,200);
    Beep(440,200);
    Beep(493.88,200);
    Beep(783.99,200);
    Beep(440,200);
    Beep(783.99,200);
    Beep(1568,200);
    Beep(392,200);

    Beep(349.23,200);
    Beep(392,200);
    Beep(440,00);
    Beep(698.46,200);
    Beep(659.25,200);
    Beep(698.46,200);
    Beep(739.99,200);
    Beep(783.99,200);
    Beep(392,200);
    Beep(392,200);
    Beep(392,200);
    Beep(392,200);
    Beep(196,200);
    Beep(196,200);
    Beep(196,200);

    Beep(185,200);
    Beep(196,200);
    Beep(185,200);
    Beep(196,200);
    Beep(207.65,200);
    Beep(220,200);
    Beep(233.08,200);
    Beep(246.94,200);

    while (true) {;}

    return (EXIT_SUCCESS);
}

