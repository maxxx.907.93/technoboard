/* The MIT License (MIT)

Copyright (c) 2015 Jean-Sebastien Castonguay

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

#include <stdio.h>
#include <stdlib.h>
#include "technoboard.h"

#define PERIODE  10
#define SEUIL    50


int main(int argc, char** argv) {

    int mesureLedAllumee;
    int mesureLedEteinte;

    TechnoBoard_Initialise(TECHNOBOARD_ALL_FEATURES);

    LED_Init();
    ADC_Start();
    GPIO_P4_1_DIR = GPIO_OUTPUT;

    while (true) {

        TechnoBoard_CyclicProcess();

        GPIO_P4_1_WRITE = GPIO_HIGH;
        Timer_Wait(PERIODE);
        mesureLedAllumee = ADC_GetRawValue(ADC_P2_3);

        GPIO_P4_1_WRITE = GPIO_LOW;
        Timer_Wait(PERIODE);
        mesureLedEteinte = ADC_GetRawValue(ADC_P2_3);

        if ((mesureLedAllumee - mesureLedEteinte) > SEUIL) {
            LED_STATE = LED_OFF;
        } else {
            LED_STATE = LED_ON;
        }
    }

    return (EXIT_SUCCESS);
}

