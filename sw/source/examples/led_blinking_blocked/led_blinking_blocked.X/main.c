#include <stdio.h>
#include <stdlib.h>
#include "technoboard.h"


int main(int argc, char** argv) {


    TechnoBoard_Initialise(TECHNOBOARD_ALL_FEATURES);
    LED_Init();

    while (true) {

        TechnoBoard_CyclicProcess();
        Timer_Wait(1000);
        LED_ToggleState();


    }

    return (EXIT_SUCCESS);
}

