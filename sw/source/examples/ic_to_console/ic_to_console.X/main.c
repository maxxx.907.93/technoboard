#include <stdio.h>
#include <stdlib.h>
#include "technoboard.h"

#define MODULE  IC_MODULE_2

int main(int argc, char** argv) {

    Timer_ms lastOutputTime = 0;

    TechnoBoard_Initialise(TECHNOBOARD_ALL_FEATURES);
    IC_Start( MODULE, IC_INPUT_P4_10, IC_FALLING_AND_RISING_EDGES);

    while (true) {

        TechnoBoard_CyclicProcess();

        if (Timer_GetElapseTime( lastOutputTime) >= 100) {

            lastOutputTime = Timer_GetTime();

            USB_PrintString("IC delta f: ");
            if (IC_IsEventDeltaTimeValid(MODULE)) {

                uint32_t delta = IC_GetEventsDeltaTime(MODULE);
                
                USB_PrintNumber( 1 / ((delta * IC_RESULUTION_IN_S) * 2.0), STR_FORMAT_FLOAT);

                USB_PrintString(" IC delta d: ");
                USB_PrintNumber( delta, STR_FORMAT_ULONG);

                USB_PrintString(" value: ");
                USB_PrintNumber( IC_GetLastEventTime(MODULE), STR_FORMAT_ULONG);
            }
            USB_PrintString("\n\r");
        }
    }

    return (EXIT_SUCCESS);
}

